import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReportsService } from './services/reports/reports.service';
import { GatewaysService } from './services/gateways/gateways.service';
import { ProjectsService } from './services/projects/projects.service';
import { ReportsComponent } from './components/reports/reports.component';
import { ReportComponent } from './components/report/report.component';
import { CardComponent } from './components/card/card.component';
import { NoDataComponent } from './components/no-data/no-data.component';
import { ReportPaymentsGroupComponent } from './components/report-payments-group/report-payments-group.component';
import { ReportPaymentsTableComponent } from './components/report-payments-table/report-payments-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ReportsComponent,
    ReportComponent,
    CardComponent,
    NoDataComponent,
    ReportPaymentsGroupComponent,
    ReportPaymentsTableComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [GatewaysService, ProjectsService, ReportsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
