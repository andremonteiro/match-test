import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPaymentsGroupComponent } from './report-payments-group.component';

describe('ReportPaymentsGroupComponent', () => {
  let component: ReportPaymentsGroupComponent;
  let fixture: ComponentFixture<ReportPaymentsGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportPaymentsGroupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportPaymentsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
