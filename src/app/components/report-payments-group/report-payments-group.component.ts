import { Component, Input } from '@angular/core';
import { GatewaysService } from 'src/app/services/gateways/gateways.service';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { Report } from 'src/app/shared/models/report';
import { Payment } from 'src/app/shared/types/reports';
import { formatCurrency } from 'src/app/shared/utils/currency';
import { getTableColumns } from 'src/app/shared/utils/table';
import { ReportPaymentsTableColumn } from '../report-payments-table/report-payments-table.component';

@Component({
  selector: 'app-report-payments-group',
  templateUrl: './report-payments-group.component.html',
  styleUrls: ['./report-payments-group.component.scss'],
})
export class ReportPaymentsGroupComponent {
  @Input() report?: Report;
  @Input() subReport?: Report;

  showTable = false;

  constructor(
    private gatewaysService: GatewaysService,
    private projectsService: ProjectsService
  ) {}

  toggleTableVisibility() {
    this.showTable = !this.showTable;
  }

  getTotalAmount() {
    return formatCurrency(this.subReport?.getTotal() ?? 0);
  }

  getTableColumns(): ReportPaymentsTableColumn<Payment>[] {
    return getTableColumns(
      this.subReport && !this.subReport.gatewayId,
      this.gatewaysService.latestSnapshotMap
    );
  }

  getTitle() {
    if (!this.report || !this.subReport) return;
    if (this.report.projectId && this.report.gatewayId) {
      return;
    }
    if (this.report.projectId && this.subReport.gatewayId) {
      return this.gatewaysService.latestSnapshotMap[this.subReport.gatewayId]
        .name;
    }
    return this.projectsService.latestSnapshotMap[this.subReport.projectId!]
      .name;
  }
}
