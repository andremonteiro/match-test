import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPaymentsTableComponent } from './report-payments-table.component';

describe('ReportPaymentsTableComponent', () => {
  let component: ReportPaymentsTableComponent;
  let fixture: ComponentFixture<ReportPaymentsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportPaymentsTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportPaymentsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
