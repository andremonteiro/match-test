import { Component, Input } from '@angular/core';

export type ReportPaymentsTableColumn<T> = {
  header: string;
  getter: (val: T) => string;
};

@Component({
  selector: 'app-report-payments-table',
  templateUrl: './report-payments-table.component.html',
  styleUrls: ['./report-payments-table.component.scss'],
})
export class ReportPaymentsTableComponent<T> {
  @Input() columns?: ReportPaymentsTableColumn<T>[] = [
    {
      header: 'Date',
      getter: (val) => (val as any).date,
    },
  ];
  @Input() rows?: T[] = [
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
    { date: '01/01/2021' },
  ] as any[];
}
