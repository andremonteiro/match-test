import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GatewaysService } from 'src/app/services/gateways/gateways.service';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { Report } from 'src/app/shared/models/report';
import { Gateway } from 'src/app/shared/types/gateways';
import { Project } from 'src/app/shared/types/projects';
import { Payment } from 'src/app/shared/types/reports';
import { formatCurrency } from 'src/app/shared/utils/currency';
import { getTableColumns } from 'src/app/shared/utils/table';
import { ReportPaymentsTableColumn } from '../report-payments-table/report-payments-table.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnChanges {
  @Input() report?: Report;

  title?: string;
  subReports?: Report[];
  tableColumns = getTableColumns();

  constructor(
    private gatewaysService: GatewaysService,
    private projectsService: ProjectsService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      JSON.stringify(changes['report'].previousValue) !==
      JSON.stringify(changes['report'].currentValue)
    ) {
      this.generateTitle();
      this.generateSubReports();
    }
  }

  getTotalAmountString() {
    let label = 'TOTAL: ';
    if (this.report?.gatewayId && !this.report.projectId) {
      label = 'GATEWAY TOTAL: ';
    }
    if (this.report?.projectId && !this.report.gatewayId) {
      label = 'PROJECT TOTAL: ';
    }

    return label + formatCurrency(this.report?.getTotal() ?? 0);
  }

  private generateTitle() {
    let title: string = this.report?.projectId
      ? this.projectsService.latestSnapshotMap[this.report.projectId].name
      : 'All projects';
    title += ' | ';
    title += this.report?.gatewayId
      ? this.gatewaysService.latestSnapshotMap[this.report.gatewayId].name
      : 'All gateways';
    this.title = title;
  }

  private generateSubReports() {
    if (!this.report) {
      this.subReports = [];
      return;
    }
    if (!this.report.projectId) {
      console.log('project subreports');
      this.subReports = this.report?.getAllProjectSubReports();
      return;
    }
    console.log('gateway subreports');
    this.subReports = this.report?.getAllGatewaySubReports();
  }
}
