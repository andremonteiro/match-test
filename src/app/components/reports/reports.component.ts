import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { FiltersService } from 'src/app/services/filters/filters.service';
import { GatewaysService } from 'src/app/services/gateways/gateways.service';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { ReportsService } from 'src/app/services/reports/reports.service';
import { Report } from 'src/app/shared/models/report';
import { Gateway } from 'src/app/shared/types/gateways';
import { Project } from 'src/app/shared/types/projects';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  report: Report | undefined;

  gateways: Gateway[] = [];
  projects: Project[] = [];

  constructor(
    public filtersService: FiltersService,
    private gatewaysService: GatewaysService,
    private projectsService: ProjectsService,
    private reportsService: ReportsService
  ) {}

  ngOnInit() {
    forkJoin([
      this.gatewaysService.getGateways(),
      this.projectsService.getProjects(),
    ]).subscribe(([gateways, projects]) => {
      this.gateways = gateways;
      this.projects = projects;

      this.generateReport();
    });
  }

  generateReport() {
    this.reportsService.generateReport().subscribe((report) => {
      this.report = report;
    });
  }

  onProjectSelected(event: Event) {
    this.filtersService.projectId = (event.target as HTMLSelectElement).value;
  }

  onGatewaySelected(event: Event) {
    this.filtersService.gatewayId = (event.target as HTMLSelectElement).value;
  }

  onDateFromChanged(event: Event) {
    this.filtersService.from = (event.target as HTMLInputElement).value;
  }

  onDateToChanged(event: Event) {
    this.filtersService.to = (event.target as HTMLInputElement).value;
  }
}
