import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FiltersService {
  private _projectId?: string;
  public get projectId(): string | undefined {
    return this._projectId;
  }
  public set projectId(value: string | undefined) {
    this._projectId = value;
  }

  private _gatewayId?: string;
  public get gatewayId(): string | undefined {
    return this._gatewayId;
  }
  public set gatewayId(value: string | undefined) {
    this._gatewayId = value;
  }

  private _from?: string;
  public get from(): string | undefined {
    return this._from;
  }
  public set from(value: string | undefined) {
    this._from = value;
  }

  private _to?: string;
  public get to(): string | undefined {
    return this._to;
  }
  public set to(value: string | undefined) {
    this._to = value;
  }

  constructor() {}
}
