import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';
import { API_BASE_URL } from 'src/app/shared/constants/api';
import { ApiResponse } from 'src/app/shared/types/api';
import { Gateway } from 'src/app/shared/types/gateways';
import { toLookupMap } from 'src/app/shared/utils/lookupmap';

@Injectable({
  providedIn: 'root',
})
export class GatewaysService {
  latestSnapshotMap: Record<string, Gateway> = {};

  constructor(private httpClient: HttpClient) {}

  getGateways() {
    return this.httpClient
      .get<ApiResponse<Gateway[]>>(new URL('gateways', API_BASE_URL).toString())
      .pipe(
        map((res) => res.data),
        tap((data) => {
          this.latestSnapshotMap = data ? toLookupMap(data, 'gatewayId') : {};
        })
      );
  }
}
