import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';
import { API_BASE_URL } from 'src/app/shared/constants/api';
import { ApiResponse } from 'src/app/shared/types/api';
import { Project } from 'src/app/shared/types/projects';
import { toLookupMap } from 'src/app/shared/utils/lookupmap';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  latestSnapshotMap: Record<string, Project> = {};

  constructor(private httpClient: HttpClient) {}

  getProjects() {
    return this.httpClient
      .get<ApiResponse<Project[]>>(new URL('projects', API_BASE_URL).toString())
      .pipe(
        map((res) => res.data),
        tap((data) => {
          this.latestSnapshotMap = data ? toLookupMap(data, 'projectId') : {};
        })
      );
  }
}
