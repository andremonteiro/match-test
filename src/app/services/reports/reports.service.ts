import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { API_BASE_URL } from 'src/app/shared/constants/api';
import { Report } from 'src/app/shared/models/report';
import { ApiResponse } from 'src/app/shared/types/api';
import { Payment } from 'src/app/shared/types/reports';
import { FiltersService } from '../filters/filters.service';

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  constructor(
    private httpClient: HttpClient,
    private filtersService: FiltersService
  ) {}

  getPayments({
    from = '2021-01-01',
    to = '2021-12-31',
    projectId,
    gatewayId,
  }: {
    from?: string;
    to?: string;
    projectId?: string;
    gatewayId?: string;
  }) {
    return this.httpClient
      .post<ApiResponse<Payment[]>>(
        new URL('report', API_BASE_URL).toString(),
        {
          from,
          to,
          projectId,
          gatewayId,
        }
      )
      .pipe(map((res) => res.data));
  }

  generateReport() {
    const filters = {
      projectId: this.filtersService.projectId,
      gatewayId: this.filtersService.gatewayId,
      from: this.filtersService.from,
      to: this.filtersService.to,
    };

    return this.getPayments(filters).pipe(
      map(
        (payments) =>
          new Report({
            ...filters,
            payments,
          })
      )
    );
  }
}
