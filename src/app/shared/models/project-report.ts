import { Project } from '../types/projects';
import { Payment } from '../types/reports';
import { Report, ReportConstructorParams } from './report';

export class ProjectReport extends Report {
  project: Project;

  constructor({
    project,
    ...params
  }: { project: Project } & ReportConstructorParams) {
    super(params);
    this.project = project;
  }
}
