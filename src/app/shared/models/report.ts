import { Project } from '../types/projects';
import { Payment } from '../types/reports';

export interface ReportConstructorParams {
  projectId?: string;
  gatewayId?: string;
  from?: string;
  to?: string;
  payments: Payment[];
}

export class Report {
  projectId?: string;
  gatewayId?: string;
  from?: string;
  to?: string;
  payments: Payment[] = [];

  constructor(params: ReportConstructorParams) {
    Object.assign(this, params);
  }

  hasData() {
    return this.payments.length > 0;
  }

  getTotal() {
    return this.payments.reduce((acc, curr) => acc + curr.amount, 0);
  }

  getPaymentsSortedByDate() {
    return this.payments.sort((a, b) => a.created.localeCompare(b.created));
  }

  private getPaymentsGroupedByProperty(
    property: Extract<keyof Payment, 'projectId' | 'gatewayId'>
  ) {
    const groups: { [key: string]: Payment[] } = {};
    for (const payment of this.payments) {
      const key = payment[property];
      if (groups[key]) {
        groups[key].push(payment);
      } else {
        groups[key] = [payment];
      }
    }
    return groups;
  }

  getPaymentsGroupedByProjectId() {
    return this.getPaymentsGroupedByProperty('projectId');
  }

  getPaymentsGroupedByGatewayId() {
    return this.getPaymentsGroupedByProperty('gatewayId');
  }

  getProjectSubReport(projectId: string) {
    return new Report({
      from: this.from,
      to: this.to,
      gatewayId: this.gatewayId,
      projectId: projectId,
      payments: this.payments.filter(
        (payment) => payment.projectId === projectId
      ),
    });
  }

  getAllProjectSubReports() {
    const paymentsGroupedByProjectId = this.getPaymentsGroupedByProjectId();
    const projectIds = Object.keys(paymentsGroupedByProjectId);
    return projectIds.map(
      (projectId) =>
        new Report({
          from: this.from,
          to: this.to,
          gatewayId: this.gatewayId,
          projectId: projectId,
          payments: paymentsGroupedByProjectId[projectId],
        })
    );
  }

  getGatewaySubReport(gatewayId: string) {
    return new Report({
      from: this.from,
      to: this.to,
      gatewayId: gatewayId,
      projectId: this.projectId,
      payments: this.payments.filter(
        (payment) => payment.gatewayId === gatewayId
      ),
    });
  }

  getAllGatewaySubReports() {
    const paymentsGroupedByGatewayId = this.getPaymentsGroupedByGatewayId();
    const gatewayIds = Object.keys(paymentsGroupedByGatewayId);
    return gatewayIds.map(
      (gatewayId) =>
        new Report({
          from: this.from,
          to: this.to,
          gatewayId: gatewayId,
          projectId: this.projectId,
          payments: paymentsGroupedByGatewayId[gatewayId],
        })
    );
  }
}
