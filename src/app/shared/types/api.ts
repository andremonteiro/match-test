export interface ApiResponse<T = any> {
  code: number;
  data: T;
  error: any;
}
