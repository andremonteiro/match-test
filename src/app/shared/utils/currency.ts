export function formatCurrency(value: number) {
  return Intl.NumberFormat(undefined, {
    style: 'currency',
    currency: 'USD',
    currencyDisplay: 'code',
  }).format(value);
}
