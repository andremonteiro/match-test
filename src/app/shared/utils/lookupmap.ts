export function toLookupMap<T>(
  array: Array<T>,
  byProperty: keyof T
): Record<string, T> {
  return array.reduce<{ [key: string]: T }>((map, item) => {
    const key = `${item[byProperty]}`;
    map[key] = item;
    return map;
  }, {});
}
