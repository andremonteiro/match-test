import { ReportPaymentsTableColumn } from 'src/app/components/report-payments-table/report-payments-table.component';
import { Gateway } from '../types/gateways';
import { Payment } from '../types/reports';
import { formatCurrency } from './currency';

export function getTableColumns(
  showGatewayColumn: boolean = false,
  gatewaysMap?: Record<string, Gateway>
): ReportPaymentsTableColumn<Payment>[] {
  return [
    {
      header: 'Date',
      getter: (payment) => payment.created,
    },
    ...(showGatewayColumn
      ? [
          {
            header: 'Gateway',
            getter: (payment: Payment) =>
              gatewaysMap?.[payment.gatewayId].name || payment.gatewayId,
          },
        ]
      : []),
    {
      header: 'Transaction ID',
      getter: (payment) => payment.paymentId,
    },
    {
      header: 'Amount',
      getter: (payment) => formatCurrency(payment.amount),
    },
  ];
}
